#!/usr/bin/python3

import os
import threading
import subprocess
import queue
import re
import json
import argparse

from getpass import getpass
from collections import OrderedDict

class FirmUpgrader():

    def __init__(self,configuration_file="./firmupgrade_config.json",n_threads=71):
        self.servers_queue = queue.Queue()
        self.result = OrderedDict({})
        self.threads = []
        self.n_threads = n_threads
        self.configuration_file = configuration_file
        self.server_lock_file = "./server_list.lock"

    def _execute_command(self,command):
        print ("- Executing command: {}".format(command))
        cmd = subprocess.Popen(command, shell=True, executable='/bin/bash', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        cmd.wait()
        stdout,stderr = cmd.communicate()

        return {
            "stdout":stdout.decode("utf-8") ,
            "stderr":stderr.decode("utf-8") ,
            "returncode":cmd.returncode
        }

    def _worker(self,task):
        def _wrapper():
            while True:
                try:
                    hostname = self.servers_queue.get()
                    if hostname is None or not hostname:
                        break
                    print ("THREAD: {}, WORKER: {} - STARTED".format(threading.currentThread().name, hostname))
                    self.result[hostname] = task(hostname)   
                except Exception:
                    break

            self.servers_queue.task_done()
        return _wrapper
    
    
    def check_insert_IPMI_password(self):
        """Check if a virtual environment SDPENV is inserted with a password. If no, then it will ask for it.
        """        
        env_name = "SDPENV"
        if env_name not in os.environ:
            password = getpass("Password is not set inside SDPENV environment variable... Please introduce the password:\n")
            os.environ[env_name] = password


    def get_server_csm(self,hostname):
        """Get the BMC domain name of a specific server.

        Args:
            hostname (str): Name of the server that will be upgraded.

        Returns:
            str: Domain name of a server.
        """        
        hostname = hostname.lower()
        return hostname.replace('cs','csm').rstrip('\n')

    def get_servers(self,server_list_file):
        """Extracts servers from a given file.

        Args:
            server_list_file (str): Path of the file where the function will extract servers.

        Returns:
            list(str): Return a list of servers.
        """        
        servers = []
        with open(server_list_file) as f:
            for line in f:
                server = line.rstrip('\n').lower()
                servers.append(server)
        return servers

    def lock_server(self,hostname):
        """Save a server in a .lock file so the upgrade process will be not executed again for this server.

        Args:
            hostname (str): Name of the server that will be locked.
        """

        with open(self.server_lock_file, "a") as lock_file:
            lock_file.write(hostname+"\n")


    def filter_servers(self,server_list):
        """Filter a server_list with the locked server list, so operation will not be executed for servers that belongs to the server lock file.

        Args:
            server (List[str]): Filtered server list
        """      
        if os.path.exists(self.server_lock_file):
            with open(self.server_lock_file) as lock_file:
                for line in lock_file:
                    line = line.strip()
                    if line in server_list:
                        server_list.remove(line)

        return server_list

    # def check_server_firmware(self,hostname):
    #     hostname = self.get_server_csm(hostname)
    #     result = self._execute_command("/usr/local/SDPTool/SDPTool {} root $SDPENV systeminfo".format(hostname))
    #     print ('\nProcess: {}, server: {} : \n{} \n{} \n'.format(threading.currentThread().name, hostname, result["stdout"].strip(), result["stderr"].strip()))
    #     return result["stdout"]
    

    def upgrade_firmware_server(self,hostname):
        """Upgrade the firmware of one server if an upgrade file is found.

        Args:
            hostname (str): Name of thet server that will be upgraded.

        Returns:
            dict: Result of the upgrade process. Stdout attribute inside the dict will print the result whilst stderr will print an error if it exists during its execution.
        """        
        try:
            file_upgrade = self.get_firmware_upgrade_file(hostname)
            if file_upgrade:
                self.lock_server(hostname)
                hostname = self.get_server_csm(hostname)
                command = {"stdout": "/usr/local/SDPTool/SDPTool {} root $SDPENV update {} -no_user_interaction".format(hostname,file_upgrade),"stderr":""}
                # result = self._execute_command("/usr/local/SDPTool/SDPTool {} root $SDPENV update {} -no_user_interaction".format(hostname,file_upgrade))
                # print ('- Process: {}, server: {} : \n{} \n{}'.format(threading.currentThread().name, hostname, result["stdout"].strip(), result["stderr"].strip()))
                # return result
                return command
            # else:
            #     return {"stdout":"","stderr":file_upgrade}
        except Exception as ex:
            return {"stdout":"","stderr":str(ex)} 

        

    def execute_server_ipmi_command(self,hostname,command):
        """Executes an ipmi command.

        Args:
            hostname (str): Name of the host that will be connected using the ipmi protocol.
            command ([type]): Ipmitool command that will be executed in the server introduced as first argument.

        Returns:
            str: Result of the ipmi command.
        """        
        hostname = self.get_server_csm(hostname)
        command = "ipmitool -I lanplus -H {} -U root -P $SDPENV -C 17 -N 5 {}".format(hostname,command)
        result = self._execute_command(command)
        if result["stderr"]:
            raise Exception(result["stderr"])
        return result["stdout"]


    def check_server_up(self,hostname):
        """Check if a server is up using fping.

        Args:
            hostname (str): Name of the server.

        Returns:
            bool: True if up.
        """        
        return self._execute_command("fping {}".format(hostname))["returncode"]



    def get_firmware_upgrade_file(self,hostname):
        """Get the path of the firmware that will be upgraded depending on the actual firmware version and model. In case nothing to do it will return False and an explanation.

        Args:
            hostname (str): Server 

        Returns:
            (bool,str): bool:Is there any firmware to be upgraded in this server? str: Firmware file path or error message in case of False.
        """ 
        configuration = {
            "model_type":{
                "previousFirmware":"fileToUpgrade"
            }
        }
        if os.path.exists(self.configuration_file):
            with open(self.configuration_file) as f:
                configuration = json.load(f)
        else:
            with open(self.configuration_file, "w") as outfile:
                json.dump(configuration, outfile)

        server_bmc_fru = self.execute_server_ipmi_command(hostname,"fru")
        server_bmc_info = self.execute_server_ipmi_command(hostname,"bmc info")
        
        product_model = re.search("Board Product.*?: (.+)",server_bmc_fru).group(1)
        firmware_version = re.search("Firmware Revision.*?: (.+)",server_bmc_info).group(1)

        if product_model in configuration:
            if firmware_version in configuration[product_model]:

                if self.check_server_up(hostname):
                    raise Exception("An update has been detected but the server needs to be turned off first. Model:{}/Firmware:{} -> Upgrade: {}".format(product_model,firmware_version,configuration[product_model][firmware_version]))
                else:
                    return configuration[product_model][firmware_version]

        raise Exception("No upgrade file for this firmware. Model:{}/Firmware:{}".format(product_model,firmware_version))




    def upgrade_firmware_servers(self,server_list_file,**args):
        """Start the firmware upgrade for a list of servers contained in a file. The process will be executed in parallel, using the number of threads defined as a attribute in this class.

        Args:
            server_list_file (str): Path to the file that contains the list of servers that will be processed for the firmware upgrade.

        Returns:
            json: A dictionary with the result obtained for the upgrade of each server.
        """
        if args["server_list"] is None:
            servers = self.get_servers(server_list_file)
        else:
            servers = args["server_list"]

        servers = self.filter_servers(servers)

        for server in servers: self.servers_queue.put(server)

        self.threads = [threading.Thread(target=self._worker(self.upgrade_firmware_server)) for i in range(self.n_threads)] 

        for thread in self.threads: 
            thread.start()
            self.servers_queue.put(None)
        
        for thread in self.threads: 
            thread.join()

        log = open('firmupgrade' + '.log', 'w')
        for server in self.result: 
            log.write("{}: {} {}\n".format(server,str(self.result[server]["stdout"]).strip(),str(self.result[server]["stderr"]).strip()))
        
        print("\nRESULT:\n"+json.dumps(self.result,indent=4))
        
        return self.result


def arguments():
    """Introduces the arguments required by the script.

    Returns:
        json: A dictionary with collected arguments either from argument defaults or from user input.
    """    
    parser = argparse.ArgumentParser(allow_abbrev=False, description="A tool that upgrades the BMC firmware of a list of servers.")
    # subparsers = parser.add_subparsers()

    # subparser = subparsers.add_parser(
    #     "start_upgrade_servers", help="Upgrade firmware of servers based on a configuration file."
    # )
    # subparser.set_defaults(function=firmUpgrader.upgrade_firmware_servers)
    parser.add_argument(
        "-slf","--server_list_file", type=str, help="(str): Path to the file that contains the list of servers that will be processed for the firmware upgrade.",default ="./server_list"
    )
    parser.add_argument(
        "-cf","--configuration_file", type=str, help="(str): Path of the file that has the necessary configuration to proceed with firmware upgrades. This configuration file will be a json that contains as keys the name of the models, and as a value another dictionary that will contain the firmware number as key and the file witch will be used to do the upgrade from this firmware. Example:  { 'model_type':{ 'previousFirmwareVersion':'fileToUpgrade' } }",default ="./firmupgrade_config.json"
    )
    parser.add_argument(
        "-nt","--n_threads", type=int, help="(int): Thread number",default=71
    )
    parser.add_argument(
        "-sl","--server_list", type=str,nargs="+", help="(List[str]): Instead of a server list you can introduce a server list by command line using this argument.",default=None
    )
    return vars(parser.parse_args())


def main():
    args = arguments()
    firmUpgrader = FirmUpgrader(configuration_file=args["configuration_file"],n_threads=args["n_threads"])
    firmUpgrader.check_insert_IPMI_password()
    firmUpgrader.upgrade_firmware_servers(**args)

if __name__ == "__main__":
    main()