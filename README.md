A tool that upgrades the BMC firmware of a list of servers

For information of how to use the tool, please execute:
`./firmupgrader -h`
